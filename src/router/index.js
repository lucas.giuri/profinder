import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Profile from '@/components/Profile'
import Register from '@/components/Register'
import VueResource from "vue-resource"

Vue.use(VueResource);
Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login
        },
        {
            path: '/Profile',
            name: 'Profile',
            component: Profile
        },
        {
            path: '/Register',
            name: 'Register',
            component: Register
        }
    ],
    mode: 'history'
})